'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    webpack = require('webpack'),
    webpackStream = require('webpack-stream');


gulp.task('sass', function () {
    gulp.src('app/media/sass/*.sass')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('app/media/css'))

});
gulp.task('scripts', function () {
    return gulp.src('app/media/js/app.js')
        .pipe(webpackStream({
            output: {
                filename: 'build.js',
            },
            module: {
                rules: [
                    {
                        test: /\.(js)$/,
                        exclude: /(node_modules)/,
                        loader: 'babel-loader',
                        query: {
                            presets: ['env']
                        }
                    }
                ]
            },
            externals: {
                jquery: 'jQuery'
            }
        }))
        .pipe(gulp.dest('app/media/js'))
        // .pipe(uglify())
        // .pipe(rename({ suffix: '.min' }))
        // .pipe(gulp.dest('app/media/js'));
});

gulp.task('watch', function () {
    gulp.watch('app/media/sass/*.sass', ['sass']);
    gulp.watch('app/media/js/app.js', ['scripts']);
});
