<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
        'css/reset.css',
        'css/slick.css',
        //'css/slick-theme.css',
        'css/styles.css',
    ];
    public $js = [
        //'js/TweenMax.min.js',
        //'js/ScrollMagic.min.js',
        //'js/animation.gsap.min.js',
        'js/slick.min.js',
        'js/build.js',

    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
