import '../sass/styles.sass'
import ScrollMagic from 'scrollmagic';
import Vue from 'vue'
import Calc from './Calc.vue'
import SectionAdvantages from './SectionAdvantages';
import SectionOurWorks from './SectionOurWorks';
import Features from './Features';
import SectionHowWeWork from './SectionHowWeWork';

    let controller = new ScrollMagic.Controller();

    new Features();
    new SectionAdvantages(controller);
    new SectionOurWorks(controller);
    new SectionHowWeWork(controller);




let coef = .2;

window.addEventListener('scroll', (e) =>{
        let scrolled = window.pageYOffset;
        requestAnimationFrame(() => {
        document.querySelector('.header__content-image').style.transform = 'translate3d(0,'+ Math.round(scrolled*coef) +'px, 0)';
    });
});

new Vue({
    render: h => h(Calc)
}).$mount('#app');

// let test = new Vue({
//     el: '#app',
//     data: {
//         message: 'Привет, Vue!',
//         seen: true
//     }
// })



    // $('.section__our-works__content--filter > ul > li > a').on('click', function (event) {
    //     event.preventDefault();
    //     $('.section__our-works__content--filter > ul > li > a').removeClass('filter-active');
    //     $(this).addClass('filter-active');
    // });

























