import ScrollMagic from 'scrollmagic';
import 'animation.gsap';
import TimelineMax from "TimelineMax";
import TweenMax from "TweenMax";

export default class SectionAdvantages{

    constructor(controller){
        this.controller = controller;
        this.init();
    }

    init(){

        let controller = this.controller;

        let parallaxImagesTween = new TimelineMax ()
            .add([
                TweenMax.fromTo("#parallaxText .layer1", 1, {y: 400}, {y: -200, ease: Linear.easeNone}),
                TweenMax.fromTo("#parallaxText .layer2", 1, {y: 600}, {y: -150, ease: Linear.easeNone}),
            ]);

        let parallaxImagesScene = new ScrollMagic.Scene({triggerElement: "#section__advantages", duration: 1200})
            .setTween(parallaxImagesTween)
            .addTo(controller);

        let parallaxImagesTween2 = new TimelineMax ()
            .add([
                TweenMax.fromTo("#parallaxText1 .layer1", 1, {y: 400}, {y: -200, ease: Linear.easeNone}),
                TweenMax.fromTo("#parallaxText1 .layer2", 1, {y: 600}, {y: -150, ease: Linear.easeNone})
            ]);
        let parallaxImagesScene2 = new ScrollMagic.Scene({triggerElement: "#kek", duration: 2200})
            .setTween(parallaxImagesTween2)
            .addTo(controller);
    }


}
