import * as PIXI from 'pixi.js';
import * as filters from 'pixi-filters';
import ScrollMagic from 'scrollmagic';
import 'animation.gsap';
import TimelineMax from "TimelineMax";
import TweenMax from "TweenMax";

export default class SectionHowWeWork{
    constructor(controller){
        this.init();
        this.controller = controller;
    }

    init(){
        let controller = new ScrollMagic.Controller();

        let section = document.querySelector('.step');
        let height = document.querySelector('.step').clientHeight;
        let width = section.clientWidth < 500 ? section.clientWidth : section.clientWidth / 2;

        let app = new PIXI.Application(width, height, { transparent: true,
            antialias: true});

        document.querySelector('.steps').appendChild(app.view);
            
        let style = new PIXI.TextStyle({
            fontFamily: "Roboto",
            fontWeight: 700,
            fontSize: Math.round(width) + 'px',
            fill: "#ff7675",
        });

        let text = new PIXI.Text('1', style);
        text.x = (width - text.width) / 2;
        text.y = 0;

        text.angle = 90;
        app.stage.addChild(text);

        let filter =  new filters.TwistFilter();
        filter.angle = 0;
        filter.radius = 0;
        filter.padding = height;
        filter.offset = [width/2, text.height/2];
        console.log(filter);
        text.filters = [filter];



        let scene = new ScrollMagic.Scene({
            triggerElement: 'canvas',
            triggerHook: 0,
            duration: this.getPinDuration
        })
            .setPin('canvas')
            .addTo(controller);

        let step1 = new ScrollMagic.Scene({
            triggerElement: '.step_1__content',
            triggerHook: 'onCenter'
        })
            .on('start', () => {
                text.text !== '1' ? this.changeNumber(text, filter, '1') : false
            })
            .addTo(controller);

        let step2 = new ScrollMagic.Scene({
            triggerElement: '.step_2__content',
            triggerHook: 'onCenter'
        })
            .on('start', () => {
                this.changeNumber(text, filter, '2')
            })
            .setTween(TweenMax.to('canvas', 1, { x: width, ease: Power4.easeOut }))
        .addTo(controller);

        let step3 = new ScrollMagic.Scene({
            triggerElement: '.step_3__content',
            triggerHook: 'onCenter'
        })
            .on('start', () => {
                this.changeNumber(text, filter, 3)
            })
            .setTween(TweenMax.to('canvas', 1, { x: 0, ease: Power4.easeOut }))
        .addTo(controller);

        let step4 = new ScrollMagic.Scene({
            triggerElement: '.step_4__content',
            triggerHook: 'onCenter'
        })
            .on('start', () => {
                this.changeNumber(text, filter, 4)
            })
            .setTween(TweenMax.to('canvas', 1, { x: width, ease: Power4.easeOut }))
        .addTo(controller);

        let step5 = new ScrollMagic.Scene({
            triggerElement: '.step_5__content',
            triggerHook: 'onCenter'
        })
            .on('start', () => {
                this.changeNumber(text, filter, 5)
            })
            .setTween(TweenMax.to('canvas', 1, { x: 0, ease: Power4.easeOut }))
        .addTo(controller);

        let step6 = new ScrollMagic.Scene({
            triggerElement: '.step_6__content',
            triggerHook: 'onCenter'
        })
            .on('start', () => {
                text.text !== '6' ? this.changeNumber(text, filter, '6') : false
            })
            .setTween(TweenMax.to('canvas', 1, { x: width, ease: Power4.easeOut }))
        .addTo(controller);




    }

    changeNumber(text, filter, num){
        TweenMax.to(filter, 1, { angle: 10, radius: 400, ease : Power4.easeOut, onComplete: () => {
                text.text = num.toString();
                TweenMax.to(filter, 1,{ angle: 0, radius: 0, ease : Power4.easeOut} );
            }});
    }

    getPinDuration(){
        const steps = document.querySelector('.steps').clientHeight;
        const step = document.querySelector('.step').clientHeight;

        return steps - step;
    }
}