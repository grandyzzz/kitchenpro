import ScrollMagic from 'scrollmagic';
import 'animation.gsap';
import TimelineMax from "TimelineMax";
import TweenMax from "TweenMax";

export default class SectionOurWorks{

    constructor(controller){
        this.controller = controller;
        this.init();
    }

    init(){

        let controller = this.controller;

        let slider = $('.section__our-works__content--body').slick({
            centerMode: true,
            infinite: false,
            variableWidth: true,
            variableHeight: true,
            initialSlide: 1,
            slidesToShow: 1,
            arrows: true,
            prevArrow: $('.prev'),
            nextArrow: $('.next')

        });

        let bg = '.slick-current > .work__content--item__bg';
        let overlay = '.slick-current > .work__content--item__overlay';

        let onComplete = function(){
            TweenMax.to(overlay, 2, {width: '25%', ease: Power4.easeOut});

        };

        let initSlider = new ScrollMagic.Scene({triggerElement: "#section__our-works", reverse: false})
            .setTween(TweenMax.to(bg, 1, {width: '75%', ease: Power4.easeOut, onComplete: onComplete}))
            .addTo(controller);



        slider.on('afterChange', function(event, slick, currentSlide) {
            // let bg = $('.slick-current > .work__content--item__bg');
            // let overlay = $('.slick-current > .work__content--item__overlay');

            let tl = new TimelineMax();
            tl.add([
                TweenMax.to(bg, 1, {width: '75%', ease: Power4.easeOut}),
                TweenMax.to(overlay, 2, {width: '25%', ease: Power4.easeOut})
            ]);
        });
        slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            // let bg = $('.slick-current > .work__content--item__bg');
            // let overlay = $('.slick-current > .work__content--item__overlay');

            let tl = new TimelineMax();
            tl.add([
                TweenMax.to(bg, 1, {width: '100%', ease: Power4.easeOut}),
                TweenMax.to(overlay, 2, {width: '0%', ease: Power4.easeOut})
            ]);
        });

    }

}
