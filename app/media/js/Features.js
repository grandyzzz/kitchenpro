

export default class Features{

    constructor(){
        this.init()
    }

    init(){
        this.getAnimatedScrolling();
        this.getSmoothScrolling();
    }

    getAnimatedScrolling(){

        $('a[href*="#"]')
        // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function(event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    let target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            let $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                }
            });
    }

    getSmoothScrolling(){

    }

//// FIXED NAVBAR
// $(window).scroll(function() {
//     if ($(window).scrollTop() > 50) {
//         $('.navbar').addClass('fixed');
//     } else {
//         $('.navbar').removeClass('fixed');
//     }
// });





    /*==============================================================*/
//background color slider Start
    /*==============================================================*/
// var $window = $(window),
//     $body = $('.bg-background-fade'),
//     $panel = $('.color-code');
// var scroll = $window.scrollTop() + ($window.height() / 2);
// $panel.each(function () {
//     var $this = $(this);
//     if ($this.position().top <= scroll && $this.position().top + $this.height() > scroll) {
//         $body.removeClass(function (index, css) {
//             return (css.match(/(^|\s)color-\S+/g) || []).join(' ');
//         });
//         $body.addClass('color-' + $(this).data('color'));
//     }
// });
    /*==============================================================*/
//background color slider End
    /*==============================================================*/



}