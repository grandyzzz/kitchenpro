<?php

?>
<?php $this->beginContent('@app/views/layouts/base.php'); ?>
    <div id="wrapper" >
        <header>
            <div class="header__content">
                <nav class="header__nav">
                    <div class="container">
                        <a href="/" class="header__nav__logo">
                            <?= file_get_contents('./images/logo.svg')?>
                        </a>
                        <ul class="nav__items">
                            <li><a href="#section__advantages">Почему мы?</a></li>
                            <li><a href="#section__our-works">Наши работы</a></li>
                            <li><a href="#section__contacts">Контакты</a></li>
                        </ul>
                    </div>
                </nav>
                <div class="header__content__container">
                    <div class="container">
                        <div class="header__content__hero">
                            <h1>Вы забудете о проблеме как впихнуть невпихуемое</h1>
                            <h2>Делаем кухни под любое помещение и...</h2>
                            <div>
                                <a href="#" class="cta-button">Записаться на замер</a>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="header__content-image">
                    <img src="/images/bg_trimmed.png" alt="">
                </div>
            </div>
        </header>

        <main>
            <?= $content ?>
        </main>

        <footer>

        </footer>
    </div>

<?php $this->endContent(); ?>