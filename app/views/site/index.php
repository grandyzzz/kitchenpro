<?php
use yii\helpers\Url;

$this->title = 'KitchenPro | Кухни на заказ в Калининграде';
?>
<section id="section__advantages">
    <div class="container">
        <div class="section__title">
            <h1>Почему выбирают именно нас?</h1>
            <h2>Честно <span>&</span> Прозрачно</h2>
        </div>
        <div class="section__advantages__content">
            <div class="section__advantages__content-item">
                <div class="row">
                    <div class="col-md-6 flex-container">
                        <p id="kek">Таким образом рамки и место обучения кадров позволяет выполнять важные задания по разработке позиций, занимаемых участниками в отношении поставленных задач. Товарищи! дальнейшее развитие различных форм деятельности играет важную роль в формировании существенных финансовых и административных условий.</p>
                    </div>
                    <div class="col-md-6">
                        <div id="parallaxText">
                            <img class="layer1" src="https://www.magnet.co.uk/imagevault/publishedmedia/zeigb3dj4lkb4mv0uu64/Alpine_Graphite_01_Final.jpg" alt="">
                            <img class="layer2" src="https://www.magnet.co.uk/imagevault/publishedmedia/zeigb3dj4lkb4mv0uu64/Alpine_Graphite_01_Final.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="section__advantages__content-item">
                <div class="row">
                    <div class="col-md-6">
                        <div id="parallaxText1">
                            <img class="layer1" src="https://www.magnet.co.uk/imagevault/publishedmedia/zeigb3dj4lkb4mv0uu64/Alpine_Graphite_01_Final.jpg" alt="">
                            <img class="layer2" src="https://www.magnet.co.uk/imagevault/publishedmedia/zeigb3dj4lkb4mv0uu64/Alpine_Graphite_01_Final.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 flex-container">
                        <p>Таким образом рамки и место обучения кадров позволяет выполнять важные задания по разработке позиций, занимаемых участниками в отношении поставленных задач. Товарищи! дальнейшее развитие различных форм деятельности играет важную роль в формировании существенных финансовых и административных условий.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<section id="section__our-works">
    <div class="container">
        <div class="section__title">
            <h1>Наши работы</h1>
            <h2>То, чем мы радуем клиентов</h2>
        </div>
        <div class="section__our-works__content">
<!--            <div class="section__our-works__content--filter">-->
<!--                <ul>-->
<!--                    <li><a href class="filter-active">Все</a></li>-->
<!--                    <li><a href>Категория 1</a></li>-->
<!--                    <li><a href>Категория 2</a></li>-->
<!--                    <li><a href>Категория 3</a></li>-->
<!--                </ul>-->
<!--            </div>-->
            <button class="prev" aria-label="Previous" type="button" style="">Previous</button>
            <button class="next" aria-label="Next" type="button" style="">Next</button>
            <i class="fas fa-arrow-left fa-2x"></i>
            <i class="fas fa-arrow-right"></i>
        </div>
    </div>
    <div class="section__our-works__content--body">
        <div class="work__content--item">
            <div class="work__content--item__bg" style="background-image: url('https://www.homebase.co.uk/-/media/uk%20images/homebase/kitchens/steamer/steamer_earl_grey_highres_832x544.jpg')">

            </div>
            <div class="work__content--item__overlay">

            </div>
        </div>
        <div class="work__content--item">
            <div class="work__content--item__bg" style="background-image: url('https://www.homebase.co.uk/-/media/uk%20images/homebase/kitchens/steamer/steamer_earl_grey_highres_832x544.jpg')">

            </div>
            <div class="work__content--item__overlay">

            </div>
        </div>
        <div class="work__content--item">
            <div class="work__content--item__bg" style="background-image: url('https://www.homebase.co.uk/-/media/uk%20images/homebase/kitchens/steamer/steamer_earl_grey_highres_832x544.jpg')">

            </div>
            <div class="work__content--item__overlay">

            </div>
        </div>
        <div class="work__content--item">
            <div class="work__content--item__bg" style="background-image: url('https://www.homebase.co.uk/-/media/uk%20images/homebase/kitchens/steamer/steamer_earl_grey_highres_832x544.jpg')">

            </div>
            <div class="work__content--item__overlay">

            </div>
        </div>
        

    </div>
</section>
<section id="section__how-we-work">
    <div class="container">
        <div class="section__title">
            <h1>Наш рабочий процесс</h1>
            <h2>Шаг за шагом</h2>
        </div>
    </div>
    <div class="steps">
        <div class="step step_1">
            <div class="container-fluid">
                <div class="row">
                    <div id="lol" class="col-md-6"></div>
                    <div class="col-md-6 step_1__content">CONTENT</div>
                </div>
            </div>
        </div>
        <div class="step step_2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 step_2__content">CONTENT</div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </div>
        <div class="step step_3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6 step_3__content">CONTENT</div>
                </div>
            </div>
        </div>
        <div class="step step_4">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 step_4__content">CONTENT</div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </div>
        <div class="step step_5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6 step_5__content">CONTENT</div>
                </div>
            </div>
        </div>
        <div class="step step_6">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 step_6__content">CONTENT</div>
                    <div class="col-md-6"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="calc-kitchen">
    <div id="app"></div>
</section>
<section id="section__contacts">


</section>
